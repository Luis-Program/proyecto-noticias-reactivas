import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NewsComponent } from './components/news/news.component';
import { CnewsComponent } from './components/CRUD/cnews/cnews.component';
import { UnewsComponent } from './components/CRUD/unews/unews.component';
import { DnewsComponent } from './components/CRUD/dnews/dnews.component';
import { NewsServicesService } from './services/news-services.service';
import { NotifierServicesService } from './services/notifier-services.service';
import { FormsModule, ReactiveFormsModule } from  '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RnewsComponent } from './components/CRUD/rnews/rnews.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
 
const config: SocketIoConfig = { url: 'http://localhost:8080/news', options: {} };
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NewsComponent,
    CnewsComponent,
    UnewsComponent,
    DnewsComponent,
    RnewsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SocketIoModule.forRoot(config),
    ChartsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
    
  ],
  providers: [NewsServicesService, NotifierServicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }

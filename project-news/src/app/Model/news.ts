export class news{
    
   public  idnews: number;
   public newstext: string;
   public idnotifierid: number;

  constructor(idnews:number, newstext:string, idnotifierid:number) {
        this.idnews = idnews;
        this.newstext = newstext;
        this.idnotifierid = idnotifierid;
      }

}
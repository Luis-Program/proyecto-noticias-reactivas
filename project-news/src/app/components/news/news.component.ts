import { Component, OnInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { NewsServicesService } from 'src/app/services/news-services.service';
import { Chart} from 'chart.js'
import { NotifierServicesService } from 'src/app/services/notifier-services.service';
import {NotificationService} from 'src/app/services/notification.service'
import {BehaviorSubject} from 'rxjs';
import { __values } from 'tslib';
import { news } from 'src/app/Model/news';
import { notifier } from 'src/app/Model/notifier';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit, OnDestroy {

  private mySubject: BehaviorSubject<news>; 
  public dataListNews: Array<news>;
  public dataListSubject: BehaviorSubject<news[]>;
  public dataListReporter: Array<notifier>;
  public counter:number;
  public NamesLabels: Array<String>;
  public BarChart = null;
  public DataAmountNews: Array<number>;
  
  
  constructor(private router:Router, private ServiceNews:NewsServicesService,private NotificationService: NotificationService,private ServiceReporter:NotifierServicesService ) {

    this.mySubject = new BehaviorSubject(null);
    this.dataListSubject = new BehaviorSubject(null);
    this.dataListReporter = new Array<notifier>();
    this.DataAmountNews = new Array<number>();
    this.NamesLabels = new Array<string>();
    this.counter = null; 
   }  

  ngOnInit(): void {
    
    this.initLoadData();
    this.doNotificationSubscription();
    this.doSubjectSubscription();
    this.LoadChart();
    this.LoadNamesNotifiers();
   
    
  }

  public LoadChart(){
    // Bar chart:
   this.BarChart = new Chart('barChart', {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: 'News',
            data: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            backgroundColor: 'rgba(54, 162, 235, 0.2)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 5
        }]
    },
    options: {
      
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
  
  
  }
  public LoadNamesNotifiers(){
    this.ServiceReporter.getNotifier()
    .subscribe(data =>{
      this.dataListReporter = data;
      for (let index = 0; index < this.dataListReporter.length; index++) {
        this.NamesLabels.push(this.dataListReporter[index].notifiername);
        if(this.dataListReporter[index].news.length>0){
          this.DataAmountNews.push(this.dataListReporter[index].news.length); 
        }else{
          this.DataAmountNews.push(0);
        }
        
      }
      this.BarChart.data.labels = this.NamesLabels;
      this.BarChart.data.datasets[0].data = this.DataAmountNews;
       this.BarChart.update();
    });

   
  }
  public doNotificationSubscription(): void {
    
      this.NotificationService
        .getNewsNotification()
        .subscribe((result) => {
          //actualizartabla
          this.mySubject.next(result);

        });
  }

  public initLoadData(){

    this.ServiceNews.getNews()
    .subscribe(data=>{
      this.dataListNews = data;
      this.counter = this.dataListNews.length;
    });

    this.dataListSubject
    .asObservable()
    .subscribe(result =>{
    });
    
    this.dataListSubject.next(this.dataListNews);

  }


  public doSubjectSubscription(): void {
    
    this.mySubject.subscribe((result) => {
      if(result!=null){
        this.actualizarTexto(result);
      }
    });

    this.mySubject.subscribe((result) => {
      if(result!=null){
        this.reloadCounter();
      }
    });

    this.mySubject.subscribe((result)=>{
      if(result!=null){
        this.UpdateGraph(result);
      }
    });

  }

  public UpdateGraph(result): void{
    var pos:number = 0;
    var ne:news = JSON.parse(result);
    ne.idnews = this.dataListNews[this.dataListNews.length-1].idnews+1;
    this.ServiceReporter.getNotifier()
    .subscribe(data =>{
      this.dataListReporter = data;
      for (let index = 0; index < this.dataListReporter.length; index++) {
      if(ne.idnotifierid == this.dataListReporter[index].idnotifier){
        break;
      }
      pos++;
      }
      this.DataAmountNews[pos]++;
      this.BarChart.data.datasets[0].data = this.DataAmountNews;
      this.BarChart.update();

    });

  }

  public actualizarTexto(result): void {
    if(result!=null){
      var n:news = JSON.parse(result);
      n.idnews = this.dataListNews[this.dataListNews.length-1].idnews+1;
    this.dataListNews.push(n);
    }
    
  }

  
  reloadCounter(){
    this.counter++;
  }


 back(){
    this.router.navigate([""]);
  }
  
  ngOnDestroy(): void{
    this.dataListSubject.unsubscribe();
    this.mySubject.unsubscribe();
  }


}  
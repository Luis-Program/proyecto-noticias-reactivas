import { Component, OnInit } from '@angular/core';
import { notifier } from 'src/app/Model/notifier';
import { Router } from '@angular/router';
import { NotifierServicesService } from 'src/app/services/notifier-services.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: number;
  password : string;
  passMessage: string;
  errorMessage  : string;
  emptyMessage : string;
  invalidLogin = false;
  pass = false;
  loginEmpty = false;
  Noti:notifier = new notifier();
  Notis: notifier[];
  constructor(private router:Router, private service:NotifierServicesService) { }

  ngOnInit(): void {
    this.Notis = [];
    this.service.getNotifier()
    .subscribe(data=>{
      this.Notis = data;
    })
  }

  handleLogin(username:number,password:string) {
    this.Noti = null;
    if(username != null && password != null ){
      if(this.find(username)!=null){
          this.loginEmpty = false;
          this.invalidLogin = false;
          if(this.Noti.idnotifier == username && this.Noti.notifierpassword == password){
            this.pass = false;
            localStorage.setItem("id",this.Noti.idnotifier.toString());
             this.gomain(this.Noti.idnotifier);
            }else{
            this.pass = true;
            this.passMessage = "Password Incorrect";}
      }else{this.invalidLogin = true; this.errorMessage='Id non-existent'; this.loginEmpty = false; }
  }else{
      this.loginEmpty = true;
      this.pass = false;
      this.emptyMessage = 'Empty Fields';}       
  }

  find(id:number){

    for (let index = 0; index < this.Notis.length; index++) {
      if (this.Notis[index].idnotifier == id) {
        this.Noti = this.Notis[index];
        return this.Noti;
        break;}} 
    return null;
  }

  gomain(id:number){
    this.router.navigate(["rnews"]);
  }


}

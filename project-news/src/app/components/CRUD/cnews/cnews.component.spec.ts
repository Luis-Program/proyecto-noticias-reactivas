import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnewsComponent } from './cnews.component';

describe('CnewsComponent', () => {
  let component: CnewsComponent;
  let fixture: ComponentFixture<CnewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewsServicesService } from 'src/app/services/news-services.service';
import { news } from 'src/app/Model/news';
@Component({
  selector: 'app-cnews',
  templateUrl: './cnews.component.html',
  styleUrls: ['./cnews.component.css']
})
export class CnewsComponent implements OnInit {

  private idnoti:number;
  private newnews:news = new news(null,null,null);
  public emptyMessage = "Empty Fields";
  public LoginEmpty = false;
  constructor(private router:Router, private service:NewsServicesService) { }

  ngOnInit(): void {
   
    
    let ids = localStorage.getItem("id");
    this.idnoti = +ids;

  }




  save(text:string){
    if (text != "") {
      
        this.LoginEmpty = false;

        this.newnews.newstext = text;
        this.newnews.idnotifierid = this.idnoti;
        this.service.createNews(this.newnews)
   
        .subscribe(data=>{
          
          localStorage.setItem("id",this.idnoti.toString());
          this.router.navigate(["rnews"]);
         
        });
    }else{
      this.LoginEmpty = true;
    } }

    back(){
      localStorage.setItem("id",this.idnoti.toString());
            this.router.navigate(["rnews"]);
    }
   
}

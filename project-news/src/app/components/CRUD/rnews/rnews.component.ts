import { Component, OnInit } from '@angular/core';
import { news } from 'src/app/Model/news';
import { Router } from '@angular/router';
import { NewsServicesService } from 'src/app/services/news-services.service';

@Component({
  selector: 'app-rnews',
  templateUrl: './rnews.component.html',
  styleUrls: ['./rnews.component.css']
})
export class RnewsComponent implements OnInit {
 


  private idnotifier: number;
  public newses: news[];
  private newsid: number;
  constructor(private router:Router, private service:NewsServicesService) { }

  ngOnInit(): void {
    let ids = localStorage.getItem("id");
    this.idnotifier = +ids;
    this.service.getNotifierNews(this.idnotifier)
    .subscribe(data=>{  
      this.newses = data;
    })
  }

  back(){
    localStorage.setItem("id",this.idnotifier.toString());
    this.router.navigate(["login"]);
  }

  delete(n:news){
    this.service.deleteNews(n)
    .subscribe(data=>{
      this.newses = this.newses.filter(r=>r!==n);
    })
  }

  edit(n:news){
    localStorage.setItem("idnew",n.idnews.toString());
    this.router.navigate(["unews"]); 
  }


  create(){
    localStorage.setItem("id",this.idnotifier.toString());
    this.router.navigate(["cnews"]);
  }

 



 }

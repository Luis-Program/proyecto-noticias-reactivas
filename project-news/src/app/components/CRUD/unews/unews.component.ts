import { Component, OnInit } from '@angular/core';
import { news } from 'src/app/Model/news';
import { Router } from '@angular/router';
import { NewsServicesService } from 'src/app/services/news-services.service';

@Component({
  selector: 'app-unews',
  templateUrl: './unews.component.html',
  styleUrls: ['./unews.component.css']
})
export class UnewsComponent implements OnInit {

  private idnoti: number;
  public ne: news = new news(null,null,null);
  private idnews: number;
  public LoginEmpty = false;
  public emptyMessage = "-.Debe llenar los campos.-";
  constructor(private router:Router, private service:NewsServicesService) { }

  ngOnInit(): void {

    let id = localStorage.getItem("idnew");
    this.idnews = +id;
    this.service.getNewsId(this.idnews)
    .subscribe(data=>{
      this.ne = data;
      this.idnoti = this.ne.idnotifierid;
    })  
  }


  back(){
    localStorage.setItem("id",this.idnoti.toString());
    this.router.navigate(["rnews"]);
  }

  update(n:news){
    if(n!=null){
      this.LoginEmpty = false;
      localStorage.setItem("id",n.idnotifierid.toString());
    this.service.updateNews(n)
    .subscribe(data=>{
      this.ne = data;
      this.router.navigate(["rnews"]);
    })
    }else{
      this.LoginEmpty = true;
    }
    
  }


}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { news } from '../Model/news';
import { Subject } from 'rxjs';
import { __values } from 'tslib';
@Injectable({
  providedIn: 'root'
})
export class NewsServicesService {

public dataListSubject = new Subject<news[]>();
  constructor(private http:HttpClient) {
   }

  Url='http://localhost:8080/news';


  getNotifierNews(id:number){
    return this.http.get<news[]>(this.Url+"/idnotifierid/"+id);
  }


  
    

  getNews(){
    return this.http.get<news[]>(this.Url)

  }

 

  createNews(news:news){
   return this.http.post<news>(this.Url,news);
    
  }


  getNewsId(idnews:number){
     return this.http.get<news>(this.Url+"/"+idnews);

  }
  
  

  updateNews(news:news){
    return this.http.put<news>(this.Url+"/"+news.idnews,news);
  }

  
  deleteNews(news:news){
    return this.http.delete<news>(this.Url+"/"+news.idnews);
  }



}

import { TestBed } from '@angular/core/testing';

import { NotifierServicesService } from './notifier-services.service';

describe('NotifierServicesService', () => {
  let service: NotifierServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotifierServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
